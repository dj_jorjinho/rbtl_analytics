<?php
class OauthAuthorize extends Controller{
	
	function get(){
		$this->response()->body = $this->view()->set('content'
											,$this->view('oauth',$_REQUEST));
	}
	
	function post(){
		$this->response();
		try{
			// Check if there is a valid request token in the current request
			// Returns an array with the consumer key, consumer secret, token,
			// token secret and token type.
			$rs = $this->oauth()
					->authorizeVerify();
			
			// See if the user clicked the 'allow' submit button
			// (or whatever you choose)
			$authorized = array_key_exists('auth', $_REQUEST);
			if(!$authorized){
				$this->response()->body = $this->view()->set('content'
											,$this->view('oauth',$_REQUEST));
				return;
			}
			
			// Set the request token to be authorized or not authorized
			// When there was a oauth_callback then this will
			// redirect to the consumer
			$this->oauth()
					->authorizeFinish($authorized
								,Arr::mk($_SESSION)->get('user_id'));
		}
		catch (OAuthException $e)
		{
			$this->response()->code = Response::BADREQUEST;
			$this->response()->body = $e->getMessage();
		}
	}
}
<?php

class OauthRequestToken extends Controller{
	
	function post(){
		$token = $this->oauth()
					->requestToken();
					
		if(!empty($token)){
			$this->response()->body = $token;
		}
		else{
			$this->response()->code = Response::UNAUTHORIZED;
		}
	}
	
}
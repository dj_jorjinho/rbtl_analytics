<?php
class Olap extends Controller{
    
    function get()
    {
        $response = $this->query();
        $result = $this->flattenResult($response);
        $response = array('results'=>$result);
        $this->response()->body = json_encode($response);
    }
    
    function query() {
        $get = Arr::mk($_GET);
        
        $dimensions = array_merge(
            $get->get('rows')->toArray(),
            $get->get('columns')->toArray()
        );
        $measures = $get->get('measures')->toArray();
        
        $dim = array();
        foreach ($dimensions as $dimension) {
            $dim[$dimension] = 'this.'.$dimension;
        }
        $dim = json_encode($dim);
        $dim = preg_replace("/[\/'\"]/",'',$dim);
        
        $mes = array(
            'recs' => 1,
            'mmax' => 'this.'.$measures[0],
            'mmin' => 'this.'.$measures[0],
            'msum' => 'this.'.$measures[0],
        );
        $mes = json_encode($mes);
        $mes = preg_replace("/[\/'\"]/",'',$mes);
        
        $map = <<<MAP
        function()	{
            emit(${dim}, ${mes});
        }
MAP;
    
        $reduce = <<<REDUCE
        function(key, vals)  {
            var ret = { msum: 0, recs: 0, mmin: vals[0].mmax, mmax: vals[0].mmax};
            
            for(var i = 0; i < vals.length; i++) {
                ret.msum += vals[i].msum;
                ret.recs += vals[i].recs;
                if(vals[i].mmin < ret.mmin)
                    ret.mmin = vals[i].mmin;
                
                if(vals[i].level > ret.mmax)
                    ret.mmax = vals[i].mmax;
            }
            return ret;
        }
REDUCE;

        $finalize = <<<FINAL
        function (key, val) {
            val.level = Math.ceil(val.msum / val.recs);
            return val;
        }
FINAL;

        $mongo = new Mongo("mongodb://localhost:21000", array("persist" => "x"));
        $mongoDb = $mongo->RBTL;

        $filters = $get->get('filters')->toArray();

        $map_reduce = new MongoMapReduce(
            $map, $reduce, $filters, array('replace'=>'temp'), false, $finalize, null
        );

        $response = $map_reduce->invoke($mongoDb, 'skill_matrix');

        return $mongoDb->temp->find();
    }
    
    function flattenResult($results) {
        $out = array();
        foreach ($results as $result) {
            $out[] = array_merge($result['_id'], $result['value']);
        }
        return $out;
    }
}
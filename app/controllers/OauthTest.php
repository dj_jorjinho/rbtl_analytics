<?php

class OauthTest extends Controller{
	function get(){
		
		// client consumer key/secret
		$ch = curl_init();
		curl_setopt_array($ch,array(
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_URL => 'http://tonic-server/oauth/register',
			CURLOPT_FRESH_CONNECT => 1,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_FORBID_REUSE => 1,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_POSTFIELDS => array('requester_name'=>'Admin'
										,'requester_email'=>'admin@orzare.com')
		));
		$result = curl_exec($ch);
		curl_close($ch);
		
		$json = json_decode($result);
		
		// The server description
		$server = array(
			'consumer_key' => $json->key,
			'consumer_secret' => $json->secret,
			'server_uri' => 'http://tonic-server/api/',
			'signature_methods' => array('HMAC-SHA1', 'PLAINTEXT'),
			'request_token_uri' => 'http://tonic-server/oauth/requestToken',
			'authorize_uri' => 'http://tonic-server/oauth/authorize',
			'access_token_uri' => 'http://tonic-server/oauth/accessToken'
		);
		
		$user_id = Arr::mk($_SESSION)->get('user_id');
		$consumer_key = $this->oauth()->updateServer($server,$user_id);
		
		$token = OAuthRequester::requestRequestToken($consumer_key, $user_id);
		
		// Callback to our (consumer) site, will be called when the user finished the authorization at the server
		$callback_uri = 'http://tonic-server/oauth/callback?consumer_key='
					.rawurlencode($consumer_key).'&usr_id='.intval($user_id);
		
		// Now redirect to the autorization uri and get us authorized
		if (!empty($token['authorize_uri'])){
			// Redirect to the server, add a callback to our server
			if (strpos($token['authorize_uri'], '?'))
			{
				$uri = $token['authorize_uri'] . '&'; 
			}
			else
			{
				$uri = $token['authorize_uri'] . '?'; 
			}
			$uri .= 'oauth_token='.rawurlencode($token['token']).'&oauth_callback='.rawurlencode($callback_uri);
		}
		else
		{
			// No authorization uri, assume we are authorized, exchange request token for access token
		   $uri = $callback_uri . '&oauth_token='.rawurlencode($token['token']);
		}
		
		$ch = curl_init();
		curl_setopt_array($ch,array(
			CURLOPT_POST => 0,
			CURLOPT_HEADER => 0,
			CURLOPT_URL => $uri,
			CURLOPT_FRESH_CONNECT => 1,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_FORBID_REUSE => 1,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_FOLLOWLOCATION => 1
		));
		$result = curl_exec($ch);
		curl_close($ch);
		
		$this->response()->body = $result;
	}
}
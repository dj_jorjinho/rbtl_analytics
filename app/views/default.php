<!DOCTYPE html>
<html lang="en">
<head>
	<title>Read Between The Lines</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" media="screen" href="css/bootstrap.min.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://raw.github.com/djjorjinho/meinPivot/master/meinPivot.js"></script>
    <script type="text/javascript" src="js/jsviews.js"></script>
</head>
<body>
    <form class="well form-inline">
        <label for="columns">Columns</label>
        <input id="columns" type="text" value="skill,skill_category" class="input-medium search-query">
        <label for="rows">Rows</label>
        <input id="rows" type="text" value="project,employee" class="input-medium search-query">
        <label for="measures">Measures</label>
        <input id="measures" type="text" value="level" class="input-medium search-query">
        <label for="date">Date</label>
        <input id="date" type="date" value="2012-05-01" class="input-medium search-query">
        <label for="projects">Projects</label>
        <input id="projects" type="text" value="DropGifts,Lazada,Bamarang" class="input-medium search-query">
        <button onclick="query();return false;" class="btn btn-primary">Fetch</button>
    </form><br/>
    <div id="table_container" class="row-fluid"></div>
    

<script>
function query() {
    var columns = jQuery('#columns').val().split(',');
    var rows = jQuery('#rows').val().split(',');
    var measures = jQuery('#measures').val().split(',');

    var filters = {
        'project': {'$in':jQuery('#projects').val().split(',')}
    };

    var date = jQuery('#date').val();
    if(date)
        filters['date'] = date;

    jQuery.ajax({
        url: '/olap',
        type: 'GET',
        dataType: 'json',
        data: {
            'columns': columns,
            'rows': rows,
            'measures': measures,
            'filters': filters
        },
        success: function(data, status, xhr) {
        	jQuery('#table_container table').remove();
        	
            var mp = jQuery.meinPivot({
                data: data.results,
                columns: columns,
                rows: rows,
                measures: measures,
                tableContainer: '#table_container'
            }).table();
            jQuery('#table_container table').addClass('table table-striped table-bordered table-condensed');
        }
    });
}
</script>

<script id="my_tpl" type="text/x-jquery-tmpl">
<![CDATA[
    {{=id}}
]]>
</script>
</body>
</html>
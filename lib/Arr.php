<?php

class Arr {
	
	private $arr;
	
	function __construct($array=array()){
		$this->arr = $array;
	}
	
	/**
	 * factory
	 */
	static function mk($array){
		return new Arr($array);
	}
	
	function __get($name){
		if(key_exists($name,$this->arr)) {
            if (is_array($this->arr[$name])) {
                return self::mk($this->arr[$name]);
            }
            return $this->arr[$name];
        }
		return null;
	}
	
	function get($name,$alt=null){
		$val = $this->__get($name);
		if(!empty($val))
			return $val;
		return $alt;
	}
	
	function __set($name,$value){
		$this->arr[$name] = $value;
		return $this;
	}
	
	function set($name,$value){
		return $this->__set($name,$value);
	}
	
	function path($path){
		if(empty($path))
			return $this->arr;
		
		$split = explode('.',$path);
		$key = array_shift($split);
		$val = key_exists($key,$this->arr)
				? $this->arr[$key]
				: null;
		
		while (!empty($val) AND !empty($split)) {
			$key = array_shift($split);
			$val = key_exists($key,$this->arr)
					? $this->arr[$key]
					: null;
		}
		return $val;
	}
	
	function __call($name,$args){
		return $this->__get($name);
	}
    
    function toArray() {
        if (! is_array($this->arr)) {
            return array($this->arr);
        }
        return $this->arr;
    }
}
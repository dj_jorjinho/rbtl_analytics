<?php

class RLog {
	
	private static $sock=null;
	private static $conf=null;
	
	private static $levels = array(
					'error' => 1,
					'info' => 2,
					'debug' => 3
					);
	
	/**
	 * Create socket for the remaining request.
	 *
	 */
	private static function socket()
	{
		if(!is_null(self::$sock))
			return self::$sock;
		
		$config = self::$conf;
		
		$addr = $config->host;
		$port = $config->port;
		
		if(
			$sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP) //Can create the socket
				AND socket_connect($sock, $addr, $port) //Can connect to the socket
			)
		{
			socket_set_nonblock($sock);
			//socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1);
			self::$sock = $sock;
			return $sock;
		}
		
		return false;
	}
	
	static function load($conf){
		self::$conf = $conf;
	}
	
	static function debug($data){
		self::log('debug',$data);
	}
	
	static function info($data){
		self::log('info',$data);
	}
	
	static function error($data){
		self::log('error',$data);
	}
	
	private static function log($level,$data=null){
		if(self::$levels[$level] > self::$levels[self::$conf->level])
			return;
		
		$trace = array_shift(array_slice(debug_backtrace(),2,1));
		
		$msg_struct = array(
			'timestamp' => time(),
			'hostname' => $_SERVER['SERVER_NAME'],
			'class' => $trace['class'],
			'method' => $trace['function'],
			'call' => $trace['class'].'::'.$trace['function'],
			'level' => $level,
			'data' => $data
		);
		if(! is_null($feature))
			$msg_struct['feature'] = $feature;
		
		$user_id = Arr::mk($_SESSION)->get('user_id');
		if(!is_null($user_id))
			$msg_struct['user_id'] = $user_id;
		
		$msg = json_encode($msg_struct);
		
		$sock = self::socket();
		
		@socket_write($sock, $msg, strlen($msg)); //Send data
	}

}
<?php

class Controller extends Resource{
	
	private $oauth = null;
	private $user = null;
	
	/**
	 * Load config file
	 *
	 */
	static function config($name='config'){
		return Conf::mk($name);
	}
	
	/**
	 * Response object for every request instance
	 */
	function response(){
		if(empty($this->response))
			$this->response = new Response($this->request);
			
		return $this->response;
	}
	
	/**
	 * Factory method
	 */
	static function view($name='default',$data=array()){
		include_once 'View.php';
		
		$view = new View($name,$data);
		$conf = self::config();
		return $view->setViewPath($conf->views_basedir);
	}
	
	/**
	 * Factory method
	 */
	function oauth(){
		if(!empty($this->oauth))
			return $this->oauth;
		
		$conf = self::config('oauth');
		$this->oauth = new Oauth($conf);
		return $this->oauth;
	}
	
	function logged(){
		return is_integer(Arr::mk($_SESSION)->user_id);
	}
	
}
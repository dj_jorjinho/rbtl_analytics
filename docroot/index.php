<?php
error_reporting(E_ALL|E_STRICT);

define('IN_PRODUCTION', exec('cat production_env') === 'TRUE');
session_start();

// set main include path
set_include_path(get_include_path()
			.PATH_SEPARATOR."../lib/");
/**
 * Startup libs
 */
require_once 'tonic.php';
require_once 'spyc.php';

// include more paths
$_includes = Spyc::YAMLLoad('../conf/includepath.yaml');
$_inc='';
foreach($_includes['paths'] as $path){
	$_inc .= PATH_SEPARATOR.$path;
}
set_include_path(get_include_path().$_inc);

// register main libraries
foreach($_includes['register'] as $lib){
	require_once $lib.'.php';
}

// logging
Rlog::load(Arr::mk(Conf::mk()->logging));

// service endpoints -> app routes
$_routes = Spyc::YAMLLoad('../conf/routes.yaml');

// handle request - nevermind the autoload...
$request = new Request(array('autoload' => $_routes));
try {
	
    $resource = $request->loadResource();
    $response = $resource->exec($request);

} catch (ResponseException $e) {
    $response = $e->response($request);
}
catch(Exception $e){
	echo $e->getMessage();
	die;
}
$response->output();


window.JsViews || window.jQuery && jQuery.views || function(q, y) {
    function z(a, b, c, e, d) {
        var f = (c = c || {viewsCount: 0,ctx: j.helpers}) && c.ctx;
        return {jsViews: "v1.0pre",path: b || "",itemNumber: ++c.viewsCount || 1,viewsCount: 0,tmpl: d,data: e || c.data || {},ctx: a && a === f ? f : f ? m(m({}, f), a) : a || {},parent: c}
    }
    function A(a, b, c, e, d, f, g) {
        return c ? (e ? d ? "$view." + d : c : "$data." + c) + (f || "") : g || b || ""
    }
    function I(a) {
        function b(h) {
            h -= e;
            h && g.push(a.substr(e, h).replace(S, "\\n"))
        }
        var c, e = 0, d = [], f = [], g = f, i = [, , f];
        a = a.replace(T, "\\$1");
        a.replace(t, 
        function(h, n, k, U, V, o, W, X, Y, J) {
            function Z(B, p, $, K, aa, ba, L, M, ca, da) {
                return u ? (u = !L, u ? B : '"') : v ? (v = !M, v ? B : '"') : K ? p.replace(C, A) + K : aa ? w ? "" : (D = E, "\u0008" + p + ":") : $ ? (w++, p.replace(C, A) + "(") : ca ? (w--, ")") : p ? p.replace(C, A) : ba ? "," : da ? w ? "" : D ? (D = x, "\u0008") : "," : (u = L, v = M, '"')
            }
            var D, F = "", w = 0, v = x, u = x;
            k = k || U;
            b(J);
            if (V)
                j.allowCode && g.push(["*", o.replace(ea, "$1")]);
            else if (k) {
                if (k === "else") {
                    i = d.pop();
                    g = i[2];
                    n = E
                }
                o = o ? (o + " ").replace(fa, Z).replace(ga, function(B, p) {
                    F += p + ",";
                    return ""
                }) : "";
                o = o.slice(0, -1);
                c = [k, W ? X || "none" : 
                    "", n && [], "{" + F + "_hash:'" + F + "',_path:'" + o + "'}", o];
                if (n) {
                    d.push(i);
                    i = c
                }
                g.push(c)
            } else if (Y)
                i = d.pop();
            e = J + h.length;
            if (!i)
                throw "Expected block tag";
            g = i[2]
        });
        b(a.length);
        return N(f)
    }
    function N(a) {
        var b, c, e = [], d = a.length, f = "try{var views=" + (r ? "jQuery" : "JsViews") + '.views,tag=views.renderTag,enc=views.encode,html=views.encoders.html,$ctx=$view && $view.ctx,result=""+\n\n';
        for (c = 0; c < d; c++) {
            b = a[c];
            if (b[0] === "*")
                f = f.slice(0, c ? -1 : -3) + ";" + b[1] + (c + 1 < d ? "result+=" : "");
            else if ("" + b === b)
                f += '"' + b + '"+';
            else {
                var g = b[0], 
                i = b[1], h = b[2], n = b[3];
                b = b[4];
                var k = b + '||"")+';
                h && e.push(N(h));
                f += g === "=" ? !i || i === "html" ? "html(" + k : i === "none" ? "(" + k : 'enc("' + i + '",' + k : 'tag("' + g + '",$view,"' + (i || "") + '",' + (h ? e.length : '""') + "," + n + (b ? "," : "") + b + ")+"
            }
        }
        a = new Function("$data, $view", f.slice(0, -1) + ";return result;\n\n}catch(e){return views.err(e);}");
        a.nested = e;
        return a
    }
    function ha(a) {
        return O[a] || (O[a] = "&#" + a.charCodeAt(0) + ";")
    }
    function ia(a) {
        try {
            return l(a)
        } catch (b) {
        }
        return a
    }
    var l, P, G, j, H, s, t, Q, R, m, x = false, E = true, r = q.jQuery, ja = /^[^<]*(<[\w\W]+>)[^>]*$|\{\{\! /, 
    C = /^(true|false|null|[\d\.]+)|(\w+|\$(view|data|ctx|(\w+)))([\w\.]*)|((['"])(?:\\\1|.)*\7)$/g, fa = /(\$?[\w\.\[\]]+)(?:(\()|\s*(===|!==|==|!=|<|>|<=|>=)\s*|\s*(\=)\s*)?|(\,\s*)|\\?(\')|\\?(\")|(\))|(\s+)/g, S = /\r?\n/g, ea = /\\(['"])/g, T = /\\?(['"])/g, ga = /\x08([^\x08]+)\x08/g, ka = 0, O = {"&": "&amp;","<": "&lt;",">": "&gt;"}, la = /[\x00"&'<>]/g, ma = Array.prototype.slice;
    if (r) {
        l = r;
        l.fn.extend({render: function(a, b, c, e) {
                return s(a, this[0], b, c, e)
            },template: function(a, b) {
                return l.template(a, this[0], b)
            }})
    } else {
        P = q.$;
        q.JsViews = 
        G = q.$ = l = {extend: function(a, b) {
                for (var c in b)
                    a[c] = b[c];
                return a
            },isArray: Array.isArray || function(a) {
                return Object.prototype.toString.call(a) === "[object Array]"
            },noConflict: function() {
                if (q.$ === G)
                    q.$ = P;
                return G
            }}
    }
    m = l.extend;
    m(l, {views: j = {templates: {},tags: {"if": function() {
                    var a = this._view;
                    a.onElse = function(b, c) {
                        for (var e = 0, d = c.length; d && !c[e++]; )
                            if (e === d)
                                return "";
                        a.onElse = y;
                        return s(a.data, b.tmpl, a.ctx, a)
                    };
                    return a.onElse(this, arguments)
                },"else": function() {
                    var a = this._view;
                    return a.onElse ? a.onElse(this, 
                    arguments) : ""
                },each: function() {
                    var a, b = "", c = arguments, e = c.length, d = this.tmpl, f = this._view;
                    for (a = 0; a < e; a++)
                        b += c[a] ? s(c[a], d, this.ctx || f.ctx, f, this._path, this._ctor) : "";
                    return e ? b : b + s(f.data, d, f.ctx, f, this._path, this.tag)
                },"=": function(a) {
                    return a
                },"*": function(a) {
                    return a
                }},helpers: {not: function(a) {
                    return !a
                }},allowCode: x,debugMode: E,err: function(a) {
                return j.debugMode ? "<br/><b>Error:</b> <em> " + (a.message || a) + ". </em>" : '""'
            },setDelimiters: function(a, b) {
                var c = b.charAt(0), e = b.charAt(1);
                a = "\\" + a.charAt(0) + 
                "\\" + a.charAt(1);
                b = "\\" + c + "\\" + e;
                t = a + "(?:(?:(\\#)?(\\w+(?=[!\\s\\" + c + "]))|(?:(\\=)|(\\*)))\\s*((?:[^\\" + c + "]|\\" + c + "(?!\\" + e + "))*?)(!(\\w*))?|(?:\\/([\\w\\$\\.\\[\\]]+)))" + b;
                t = RegExp(t, "g")
            },registerTags: Q = function(a, b) {
                var c;
                if (typeof a === "object")
                    for (c in a)
                        Q(c, a[c]);
                else
                    j.tags[a] = b;
                return this
            },registerHelpers: R = function(a, b) {
                if (typeof a === "object")
                    for (var c in a)
                        R(c, a[c]);
                else
                    j.helpers[a] = b;
                return this
            },encode: function(a, b) {
                return b ? (H[a || "html"] || H.html)(b) : ""
            },encoders: H = {none: function(a) {
                    return a
                },
                html: function(a) {
                    return String(a).replace(la, ha)
                }},renderTag: function(a, b, c, e, d) {
                var f, g;
                f = arguments;
                g = j.presenters;
                hash = d._hash;
                tagFn = j.tags[a];
                if (!tagFn)
                    return "";
                e = e && b.tmpl.nested[e - 1];
                d.tmpl = d.tmpl || e || y;
                if (g && g[a]) {
                    g = m(m({}, d.ctx), d);
                    delete g.ctx;
                    delete g._path;
                    delete g.tmpl;
                    d.ctx = g;
                    d._ctor = a + (hash ? "=" + hash.slice(0, -1) : "");
                    d = m(m({}, tagFn), d);
                    tagFn = j.tags.each
                }
                d._encode = c;
                d._view = b;
                return (f = tagFn.apply(d, f.length > 5 ? ma.call(f, 5) : [b.data])) || (f === y ? "" : f.toString())
            }},render: s = function(a, b, c, e, d, 
        f) {
            var g, i, h, n, k = "";
            if (arguments.length === 2 && a.jsViews) {
                e = a;
                c = e.ctx;
                a = e.data
            }
            b = l.template(b);
            if (!b)
                return "";
            if (l.isArray(a)) {
                n = new z(c, d, e, a);
                g = 0;
                for (i = a.length; g < i; g++) {
                    h = (h = a[g]) ? b(h, new z(c, d, n, h, b, this)) : "";
                    k += j.activeViews ? "<!--item--\>" + h + "<!--/item--\>" : h
                }
            } else
                k += b(a, new z(c, d, e, a, b));
            return j.activeViews ? "<!--tmpl(" + (d || "") + ") " + (f ? "tag=" + f : b._name) + "--\>" + k + "<!--/tmpl--\>" : k
        },template: function(a, b) {
            if (b) {
                if ("" + b === b)
                    b = I(b);
                else if (r && b instanceof l)
                    b = b[0];
                if (b) {
                    if (r && b.nodeType)
                        b = l.data(b, 
                        "tmpl") || l.data(b, "tmpl", I(b.innerHTML));
                    j.templates[b._name = b._name || a || "_" + ka++] = b
                }
                return b
            }
            return a ? "" + a !== a ? a._name ? a : l.template(null, a) : j.templates[a] || l.template(null, ja.test(a) ? a : ia(a)) : null
        }});
    j.setDelimiters("{{", "}}")
}(window);
window.tmpl = function(id, obj) {
    var html = jQuery.trim(document.getElementById(id).text);
    if (html == undefined)
        return '';
    html = html.replace(/^\<\!\[CDATA\[/, '').replace(/\]\]\>$/, '');
    if (obj)
        return jQuery('<div>' + html + '</div>').render(obj);
    return html;
};
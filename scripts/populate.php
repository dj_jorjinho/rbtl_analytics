<?php
class PopulateMongo {
    protected $_config = array(
        'mongo_connect' => "mongodb://localhost:21000",
        'db' => 'RBTL',
        'collection' => 'skill_matrix',
        
        'projects' => array('DropGifts', 'Lazada', 'Bamarang'),
        'skills' => array('SQL'=>'Web', 'PHP5'=>'Web', 'Doctrine'=>'Web', 'Zend 1'=>'Web', 'Web Analytics'=>'Web', 'Android'=>'Mobile'),
        'project_managers' => array('Luis', 'Ireneu', 'João'),
        'team_leaders' => array('André', 'Daniel', 'Henrique'),
        'employees' => array ('Bob', 'Billy', 'Joe', 'Larry', 'Curly', 'Moe'),
        'employees_min' => 3,
        'job_title' => array('Junior', 'Regular', 'Senior'),
        'skill_level_range' => array(1,5),
        'dates' => array('2012-02-01', '2012-03-01', '2012-04-01', '2012-05-01')
    );
    
    protected $_registry = array();

    function connect()
    {
        $mongo = new Mongo($this->_config['mongo_connect'], array("persist" => "x"));
        $this->_registry['mongo'] = $mongo;
        return $mongo;
    }
    
    function populate()
    {
        $mongo = $this->connect();
        $db_name = $this->_config['db'];
        $col_name = $this->_config['collection'];
        $db = $mongo->$db_name;
        $collection = $db->$col_name;
        $collection->remove();
        
        foreach ($this->_config['dates'] as $date) {
            
            // project / manager
            foreach ( range(0,count($this->_config['projects'])-1) as $idx) {
                $project = $this->_config['projects'][$idx];
                $manager = $this->_config['project_managers'][$idx];
                $rnd = rand(1,count($this->_config['team_leaders']));
                $leaders = array_rand($this->_config['team_leaders'], $rnd);
                
                $leaders = is_array($leaders)? $leaders: array($leaders);
                
                // team leader(s)
                foreach ($leaders as $k => $leader_idx) {
                    $leader = $this->_config['team_leaders'][$leader_idx];
                    
                    // employees
                    $employees = array_rand(
                        $this->_config['employees'],
                        rand($this->_config['employees_min'], count($this->_config['employees']))
                    );
                    
                    foreach ($employees as $employee_idx) {
                        $employee = $this->_config['employees'][$employee_idx];
                        $title = $this->_config['job_title'][array_rand($this->_config['job_title'])];
                        
                        // skills
                        foreach ($this->_config['skills'] as $skill => $cat) {
                            $level = call_user_func_array('rand',$this->_config['skill_level_range']);
                            $dt = explode('-', $date);

                            // new record
                            $record = array(
                                'project' => $project,
                                'project_manager' => $manager,
                                'date' => $date,
                                'team_leader' => $leader,
                                'employee' => $employee,
                                'job_title' => $title,
                                'skill' => $skill,
                                'skill_category' => $cat,
                                'level' => $level,
                                'year' => $dt[0],
                                'month' => $dt[1],
                                'day' => $dt[2]
                            );

                            $collection->insert($record);
                        }
                    }
                    
                }
            }
        }
        
        
    }
}

$populate = new PopulateMongo();
$populate->populate();